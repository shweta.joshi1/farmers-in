/**
 * Copyright 2020 Quantiphi Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const winston = require("winston");
const expressWinston = require("express-winston");
//const databaseConnections = require("./database");
const webhookController = require("./intent-library/webhook-controller");
const requestValidator = require("./helper/request-validator");
const errorHandler = require("./helper/error-handler");
const config = require("./config")();
const logger = require("./logger");
const authMiddleware = require("./helper/basic-auth");
const helmet = require("helmet");
const path = require("path");


module.exports = async () => {
    if(process.env.NODE_ENV == null || process.env.NODE_ENV == "local"){
        //process.env.GOOGLE_APPLICATION_CREDENTIALS = path.join(__dirname,"keys/service-account.json");
    }
    let app = express();

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(helmet());
    app.use(errorHandler);

    let router = express.Router();

    router.get("/healthcheck", (req, res) => {
        res.status(200).json({ "message": "ok" });
    });
    /*let connections = await databaseConnections();
    let db = undefined;
    if(connections["types"].length == 0){
        logger.log("info", `database: ${false}`, null);
    }
    else{
        db = connections["connection"];
        logger.log("info", `database: ${true}`, null);
        logger.log("info", `databaseType(s) : ${connections["types"]}`, null);
    }
*/

    router.post("/v2beta1/webhook", requestValidator.v2RequestValidator() ,webhookController(db));

    expressWinston.requestWhitelist.push("body");
    expressWinston.responseWhitelist.push("body");

    expressWinston.bodyBlacklist = config.logger.piiFields;

    app.use(expressWinston.logger({
        transports: [
            new winston.transports.Console()
        ],
        metaField: "apiDetails",
        format: winston.format.combine(
            winston.format.json()
        )
    }));

    if (config.auth.enable) {
        app.use(authMiddleware);
    }

    app.use("/", router);

    app.use(errorHandler);  

    return app;
};
